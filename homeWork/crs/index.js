function Human(name, age){
	this.name = name;
	this.age = age;	

}
let group = [];
group.push(new Human('Anna', 27));
group.push(new Human('Luka', 15));
group.push(new Human('Lena', 35));
group.push(new Human('Maria', 25));
group.push(new Human('Eduard', 44));

group.sort((a, b) => a.age > b.age ? 1 : -1);

group.forEach(function (element){
	console.log(element);
})  



