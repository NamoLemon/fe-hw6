function Human(name, age, city){
	this.name = name;
	this.age = age;	
	this.city = city;

}

Human.prototype.sayHello = function(){
	document.write('-Hello, '+ this.name + ' !' + '<br/>');
}

Human.prototype.question = function(){
	document.write('-Where are you from, '+ this.name + ' ?' + '<br/>' + '-I`m from ' + this.city + '<br/>');
}

var a = new Human('Anna', 27, 'Kiev');
var b = new Human('Luka', 15, 'Irpin');
var c = new Human('Lena', 35, 'Bucha');
var d = new Human('Maria', 25, 'Kiev');
var e = new Human('Eduard', 44, 'Gostomel');

a.sayHello();
b.sayHello();
c.sayHello();
d.sayHello();
e.sayHello();

e.question();
d.question();
